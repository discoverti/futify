package server

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"time"

	"github.com/librespot-org/librespot-golang/librespot"
	"github.com/nanu-c/qml-go"
	"github.com/zmb3/spotify"
	"golang.org/x/oauth2"
)

const (
	cacheSession = "session"

	tokenFutify = "ddf3c08dc7bc4d90b8f8f35d42f9b992"
)

var (
	cachePath  = "/home/phablet/.cache/futify.frenchutouch/"
	configPath = "/home/phablet/.config/futify.frenchutouch/"
)

func init() {
	tmpCachePath := os.Getenv("XDG_CACHE_HOME")
	if tmpCachePath != "" {
		cachePath = path.Join(tmpCachePath, "futify.frenchutouch")
		fmt.Println(fmt.Sprintf("Env $XDG_CACHE_HOME set use it = %s", cachePath))
	} else {
		fmt.Println(fmt.Sprintf("Env $XDG_CACHE_HOME no set use default = %s", cachePath))
	}

	tmpConfigPath := os.Getenv("$XDG_CONFIG_HOME")
	if tmpConfigPath != "" {
		configPath = path.Join(tmpConfigPath, "futify.frenchutouch")
		fmt.Println(fmt.Sprintf("Env $XDG_CONFIG_HOME set use it = %s", configPath))
	} else {
		fmt.Println(fmt.Sprintf("Env $XDG_CONFIG_HOME no set use default = %s", configPath))
	}
}

func (s *Session) TryLogin() bool {
	resp := make(chan bool, 1)
	defer close(resp)
	go func() {
		if blob, err := getFromCache(cacheSession, "current"); err == nil {
			fmt.Println("before restore session")
			parts := bytes.Split(blob, []byte("\n"))
			if len(parts) < 2 {
				resp <- false
				return
			}
			session, err := librespot.LoginSaved(string(parts[0]), parts[1], "Futify")
			fmt.Println("after restore session")
			if err == nil {
				//we are logged
				s.IsLogged = true
				s.spotSession = session
				s.Username = session.Username()

				accessToken, err := s.spotSession.Mercury().GetToken(tokenFutify, "playlist-read-collaborative,playlist-read-private,user-read-recently-played,user-top-read,user-library-read")
				if err != nil {
					fmt.Println("Can't retreive token " + err.Error())
				}
				d, _ := time.ParseDuration(fmt.Sprintf("%ds", accessToken.ExpiresIn))
				t := oauth2.Token{AccessToken: accessToken.AccessToken, Expiry: time.Now().Add(d), TokenType: accessToken.TokenType}

				client := spotify.Authenticator{}.NewClient(&t)
				s.spotAPIClient = client

				s.loadConfig()

				resp <- true
				return
			}
		}
		resp <- false
	}()
	return <-resp
}

func (s *Session) Login(username, password string) string {
	session, err := librespot.Login(username, password, "Futify")
	if err != nil {
		fmt.Println("Error during login = " + err.Error())
		return err.Error()
	}
	fmt.Println(("keep login"))
	forCache := append([]byte(fmt.Sprintf("%s\n", username)), session.ReusableAuthBlob()...)
	_, err = writeInCache(bytes.NewReader(forCache), cacheSession, "current")
	if err != nil {
		fmt.Println("Error during save login = " + err.Error())
		return "Can't save session"
	}
	s.spotSession = session
	s.IsLogged = true
	qml.Changed(s, &s.IsLogged)
	s.Username = session.Username()
	qml.Changed(s, &s.Username)
	fmt.Println("User logged with " + s.Username)
	//TODO make it a function and avoid duplicate code with tryLogin
	accessToken, err := s.spotSession.Mercury().GetToken(tokenFutify, "playlist-read-collaborative,playlist-read-private,user-read-recently-played,user-top-read,user-library-read")
	if err != nil {
		fmt.Println("Can't retreive token " + err.Error())
	}
	d, _ := time.ParseDuration(fmt.Sprintf("%ds", accessToken.ExpiresIn))
	t := oauth2.Token{AccessToken: accessToken.AccessToken, Expiry: time.Now().Add(d), TokenType: accessToken.TokenType}

	client := spotify.Authenticator{}.NewClient(&t)
	s.spotAPIClient = client
	return ""
}

func (s *Session) LoadRecentlyPlay() string {
	uuid := "RecentlyPlay"
	if s.GetAlbum(uuid) != nil {
		// already loaded don't redwnload it
		fmt.Println("Found uuid return " + uuid)
		return uuid
	}
	items, err := s.spotAPIClient.PlayerRecentlyPlayed()
	if err != nil {
		log.Fatalf("couldn't get PlayerRecentlyPlayed: %v", err)
	}
	ids := make([]spotify.ID, len(items))

	for i, track := range items {
		ids[i] = track.Track.ID
	}
	tracks, err := s.spotAPIClient.GetTracks(ids...)
	myTracks := make([]*Track, len(tracks))
	image := ""
	for index, t := range tracks {
		myTracks[index] = NewTrack(t, s)
		if image == "" {
			image = myTracks[index].Image
		}
	}
	s.albums[uuid] = NewAlbum(uuid, uuid, "", []spotify.SimpleArtist{}, image, myTracks)
	fmt.Println(fmt.Sprintf("Found %d songs added in %s", len(myTracks), uuid))
	return uuid
}

func (s *Session) LoadTopTracks() string {
	uuid := "TopTracks"
	if s.GetAlbum(uuid) != nil {
		// already loaded don't redwnload it
		return uuid
	}
	items, err := s.spotAPIClient.CurrentUsersTopTracks()
	if err != nil {
		log.Fatalf("couldn't get TopTracks: %v", err)
	}
	ids := make([]spotify.ID, len(items.Tracks))

	for i, track := range items.Tracks {
		ids[i] = track.ID
	}
	fmt.Println(fmt.Sprintf("call spotify api for %d songs", len(ids)))
	tracks, err := s.spotAPIClient.GetTracks(ids...)
	fmt.Println(fmt.Sprintf("receive %d songs", len(tracks)))
	myTracks := make([]*Track, len(tracks))
	image := ""
	for index, t := range tracks {
		myTracks[index] = NewTrack(t, s)
		if image == "" {
			image = myTracks[index].Image
		}
	}
	fmt.Println(fmt.Sprintf("append album with %d songs", len(myTracks)))
	s.albums[uuid] = NewAlbum(uuid, uuid, "", []spotify.SimpleArtist{}, image, myTracks)
	return uuid
}

func (s *Session) LoadFeaturedPlaylists() string {
	uuid := "FeaturedPlaylists"
	if s.GetPlaylists(uuid) != nil {
		// already loaded don't redwnload it
		return uuid
	}
	m, p, _ := s.spotAPIClient.FeaturedPlaylists()
	fmt.Println("Message from spotify=", m)
	playlist := make([]*Playlist, len(p.Playlists))
	for index, t := range p.Playlists {
		playlist[index] = NewPlaylist(t.ID.String(), t.Name, t.Owner.DisplayName, SelectImageURL(t.Images), 0, []*Track{})
	}
	s.playlists[uuid] = NewPlaylists(uuid, "Featured Playlist", playlist)
	return uuid
}

func (s *Session) LoadUserPlaylists() string {
	uuid := "UserPlaylists"
	if s.GetPlaylists(uuid) != nil {
		// already loaded don't redwnload it
		return uuid
	}
	p, err := s.spotAPIClient.CurrentUsersPlaylists()
	if err != nil {
		log.Fatalf("couldn't get UserTracks: %v", err)
	}
	playlist := make([]*Playlist, len(p.Playlists))
	for index, t := range p.Playlists {
		playlist[index] = NewPlaylist(t.ID.String(), t.Name, t.Owner.DisplayName, SelectImageURL(t.Images), 0, []*Track{})
	}
	s.playlists[uuid] = NewPlaylists(uuid, "Your playlists", playlist)
	return uuid
}

func (s *Session) LoadUserAlbums() string {
	uuid := "UserAlbums"
	if s.GetPlaylists(uuid) != nil {
		// already loaded don't redwnload it
		return uuid
	}
	p, err := s.spotAPIClient.CurrentUsersAlbums()
	if err != nil {
		log.Fatalf("couldn't get UserTracks: %v", err)
	}
	playlist := make([]*Playlist, len(p.Albums))
	for index, t := range p.Albums {
		playlist[index] = NewPlaylist(t.ID.String(), t.Name, t.AlbumGroup, SelectImageURL(t.Images), 0, []*Track{})
	}
	s.playlists[uuid] = NewPlaylists(uuid, "Your albums", playlist)
	return uuid
}

func (s *Session) LoadAlbum(albumUuid string) string {
	if s.GetAlbum(albumUuid) != nil {
		// already loaded don't redwnload it
		fmt.Println("Found uuid return " + albumUuid)
		return albumUuid
	}
	a, err := s.spotAPIClient.GetAlbum(spotify.ID(albumUuid))
	if err != nil {
		fmt.Println(err)
	}
	ids := make([]spotify.ID, len(a.Tracks.Tracks))

	for i, track := range a.Tracks.Tracks {
		ids[i] = track.ID
	}
	tracks, _ := s.spotAPIClient.GetTracks(ids...)
	myTracks := make([]*Track, len(tracks))
	for index, t := range tracks {
		myTracks[index] = NewTrack(t, s)
	}
	s.albums[albumUuid] = NewAlbum(albumUuid, a.Name, a.AlbumGroup, a.Artists, SelectImageURL(a.Images), myTracks)
	fmt.Println(fmt.Sprintf("Found %d songs added in %s", len(myTracks), albumUuid))
	return albumUuid
}

func (s *Session) EndQueue(t *Track) {
	s.albums["queue"].tracks = append(s.albums["queue"].tracks, t)
	s.albums["queue"].Size = len(s.albums["queue"].tracks)
}

func (s *Session) DeleteQueue(index int) {
	s.albums["queue"].tracks = append(s.albums["queue"].tracks[:index], s.albums["queue"].tracks[index+1:]...)
	s.albums["queue"].Size = len(s.albums["queue"].tracks)
}

func (s *Session) ClearQueue() {
	s.albums["queue"].tracks = make([]*Track, 0)
	s.albums["queue"].Size = 0
}

func (s *Session) LoadPlaylist(playlistUuid string) string {
	if s.GetPlaylist(playlistUuid) != nil {
		// already loaded don't redwnload it
		fmt.Println("Found uuid return " + playlistUuid)
		return playlistUuid
	}
	a, err := s.spotAPIClient.GetPlaylist(spotify.ID(playlistUuid))
	if err != nil {
		fmt.Println(err)
	}
	myTracks := make([]*Track, len(a.Tracks.Tracks))
	for index, t := range a.Tracks.Tracks {
		myTracks[index] = NewTrack(&t.Track, s)
	}
	s.playlist[playlistUuid] = NewPlaylist(playlistUuid, a.Name, a.Owner.DisplayName, SelectImageURL(a.Images), a.Followers.Count, myTracks)
	fmt.Println(fmt.Sprintf("Found %d songs added in %s", len(myTracks), playlistUuid))
	return playlistUuid
}

func (s *Session) Search(search string) *SearchResults {
	res, err := s.spotAPIClient.Search(search, spotify.SearchTypeAlbum|spotify.SearchTypeArtist|spotify.SearchTypeTrack|spotify.SearchTypePlaylist)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	s.searchResults = NewSearchResults(res, s)
	fmt.Println(fmt.Sprintf("Found results for %s", search))
	return s.searchResults
}

func (s *Session) SaveSettings(quality int, theme int, errorSong int) {
	s.Settings.Quality = quality
	s.Settings.Theme = theme
	s.Settings.ErrorSong = errorSong
	s.writeSettings()
}

func (s *Session) Logout() {
	s.IsLogged = false
	s.Username = ""
	s.Settings = &Settings{Quality: 1, Theme: 0, ErrorSong: 0}
	s.albums = make(map[string]*Album)
	s.playlists = make(map[string]*Playlists)
	s.playlist = make(map[string]*Playlist)
	s.tracks = make(map[string]*Track)
	os.Remove(path.Join(cachePath, cacheSession, "current"))
}

func writeInCache(r io.Reader, typeBin string, filename string) (string, error) {
	dir := path.Join(cachePath, typeBin)
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		return "", err
	}
	path := path.Join(dir, filename)
	fo, err := os.Create(path)
	if err != nil {
		return "", err
	}

	// close fo on exit and check for its returned error
	defer func() {
		if err := fo.Close(); err != nil {
			fmt.Println("Error during close of file cache", err)
		}
	}()
	// make a write buffer
	w := bufio.NewWriter(fo)

	// make a buffer to keep chunks that are read
	buf := make([]byte, 1024)
	for {
		// read a chunk
		n, err := r.Read(buf)
		if err != nil && err != io.EOF {
			fmt.Println("Can't read buffer for write file cache", err)
			return "", err
		}
		if n == 0 {
			break
		}

		// write a chunk
		if _, err := w.Write(buf[:n]); err != nil {
			fmt.Println("Can't write buffer for write file cache", err)
			return "", err
		}
	}

	if err = w.Flush(); err != nil {
		fmt.Println("Can't flush buffer for write file cache", err)
		return "", err
	}

	fmt.Println(fmt.Sprintf("Finishing writing %s", path))

	return path, nil
}

func getFromCache(typeBin string, filename string) ([]byte, error) {
	return ioutil.ReadFile(path.Join(cachePath, typeBin, filename))
}

func (s *Session) loadConfig() error {
	data, err := ioutil.ReadFile(path.Join(configPath, "settings.json"))
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil
		}
		return err
	}
	fmt.Println("Settings exist!")
	if err := json.Unmarshal(data, s.Settings); err != nil {
		fmt.Println("Bad formatted settings")
		return err
	}

	return nil
}

func (s *Session) writeSettings() error {
	data, err := json.Marshal(s.Settings)
	if err != nil {
		fmt.Println("Can't marshall settings")
	}
	if err := os.MkdirAll(configPath, os.ModePerm); err != nil {
		fmt.Println(fmt.Sprintf("Can't mkdirAll %s", configPath))
		return err
	}
	if err := ioutil.WriteFile(path.Join(configPath, "settings.json"), data, os.ModePerm); err != nil {
		fmt.Println("can't write settings")
		return err
	}
	return nil
}
