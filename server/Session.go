package server

import (
	"github.com/librespot-org/librespot-golang/librespot/core"
	"github.com/nanu-c/qml-go"
	"github.com/zmb3/spotify"
)

type Settings struct {
	Quality   int //0 == low, 1 == medium, 2 == high
	Theme     int //0 == dark, 1 == light, 2 == system
	ErrorSong int // 0 == silence, 1 == error
}

type Session struct {
	Root          qml.Object
	spotSession   *core.Session
	spotAPIClient spotify.Client
	IsLogged      bool
	Username      string
	Settings      *Settings
	playlists     map[string]*Playlists
	playlist      map[string]*Playlist
	albums        map[string]*Album
	searchResults *SearchResults
	tracks        map[string]*Track
	port          int
}

func NewSession(port int) *Session {
	spotSession := &Session{
		IsLogged: false,
		Username: "",
		Settings: &Settings{
			Quality: 1,
			Theme:   0,
		},
		albums:    make(map[string]*Album),
		playlists: make(map[string]*Playlists),
		playlist:  make(map[string]*Playlist),
		tracks:    make(map[string]*Track),
		port:      port,
	}
	spotSession.albums["queue"] = &Album{Size: 0, Name: "Queue", Uuid: "queue", tracks: make([]*Track, 0)}
	return spotSession
}
