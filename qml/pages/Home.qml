import QtQuick 2.12
import Ubuntu.Components 1.3
import UserMetrics 0.1

import "../components"

Page {
    id: page

    property bool needSave: false

    signal navigate(var pageName, var pageParams)
    signal startTrack(var track)
    signal startPlaylist(var playlist)
    signal startAlbum(var album)

    function init() {
        homeYourPlaylists.uuid = spotSession.loadUserPlaylists();
        homeYourAlbums.uuid = spotSession.loadUserAlbums();
        homeRecentlyPlay.uuid = spotSession.loadRecentlyPlay();
        homeFeaturedPlaylists.uuid = spotSession.loadFeaturedPlaylists();
        homeTopTracks.uuid = spotSession.loadTopTracks();
    }

    anchors.fill: parent

    header: PageHeader {
        id: pageHeader
        title: "Futify"

        StyleHints {
            dividerColor: UbuntuColors.green
        }

        TextField {
            id: inputSearch
            anchors.top: pageHeader.top
            anchors.right: pageHeader.trailingActionBar.left
            anchors.verticalCenter: pageHeader.verticalCenter
            anchors.topMargin: units.gu(1)
            anchors.bottomMargin: units.gu(1)
            opacity: 0
            onAccepted: {
                if (inputSearch.text !== "") {
                    navigate("search", {"search": inputSearch.text});
                    inputSearch.opacity = 0;
                    inputSearch.text = "";
                } else {
                    animateOpacity.from = 1;
                    animateOpacity.to = 0;
                    animateOpacity.start();
                }

            }
        }
        NumberAnimation {
            id: animateOpacity
            target: inputSearch
            properties: "opacity"
            from: 0.0
            to: 1.0
            duration: 500
            onFinished: {
                if (inputSearch.opacity > 0) {
                    inputSearch.focus = true;
                }
            }
        }

        trailingActionBar {
            numberOfSlots: 2
            actions: [
                Action {
                    text: i18n.tr("Search")
                    onTriggered: {
                        if (inputSearch.opacity == 0) {
                            animateOpacity.from = 0;
                            animateOpacity.to = 1;
                            animateOpacity.start();
                        } else {
                            animateOpacity.from = 1;
                            animateOpacity.to = 0;
                            animateOpacity.start();
                        }
                    }
                    iconName: "find"
                },
                Action {
                    text: i18n.tr("Settings")
                    onTriggered: navigate("settings", undefined)
                    iconName: "settings"
                },
                Action {
                    text: i18n.tr("About")
                    onTriggered: navigate("about", undefined)
                    iconName: "stock_document"
                }
            ]
        }
    }

    Flickable {
        id: home
        anchors.top: pageHeader.bottom
        width: parent.width
        height: parent.height - pageHeader.height
        contentWidth: parent.width
        contentHeight: columnHome.height
        clip: true

        Column {
            id: columnHome
            width: parent.width
            height: units.gu(30) * 5
            topPadding: units.gu(2)
            spacing: units.gu(6)

            HorizontalList {
                id: homeYourPlaylists
                width: parent.width

                title: i18n.tr("Your playlists")
                icon: 'view-list-symbolic'
                uuid: ''
                getSize: function(uuid) {
                    return spotSession.getPlaylists(uuid).size;
                } 
                getData: function(uuid, index) {
                    return spotSession.getPlaylistByIndex(uuid, index)
                }
                onStart: function(playlist) {
                    startPlaylist(playlist)
                }
                showInfo: true
                onGoToDetails: function(data) {
                    navigate("playlist", data);
                }
            }

            HorizontalList {
                id: homeYourAlbums
                width: parent.width

                title: i18n.tr("Your albums")
                icon: 'view-list-symbolic'
                uuid: ''
                getSize: function(uuid) {
                    return spotSession.getPlaylists(uuid).size;
                } 
                getData: function(uuid, index) {
                    return spotSession.getPlaylistByIndex(uuid, index)
                }
                onStart: function(playlist) {
                    startAlbum(playlist)
                }
                showInfo: true
                onGoToDetails: function(data) {
                    navigate("album", data);
                }
            }

            HorizontalList {
                id: homeRecentlyPlay
                width: parent.width

                title: i18n.tr("Recently Play")
                icon: 'history'
                uuid: ''
                getSize: function(uuid) {
                    return spotSession.getAlbum(uuid).size;
                } 
                getData: function(uuid, index) {
                    return spotSession.getTrackByAlbum(uuid, index)
                }
                onStart: startTrack(data)
            }

            HorizontalList {
                id: homeFeaturedPlaylists
                width: parent.width

                title: i18n.tr("Featured playlist")
                icon: 'stock_music'
                uuid: ''
                getSize: function(uuid) {
                    return spotSession.getPlaylists(uuid).size;
                } 
                getData: function(uuid, index) {
                    return spotSession.getPlaylistByIndex(uuid, index)
                }
                onStart: function(playlist) {
                    startPlaylist(playlist)
                }
                showInfo: true
                onGoToDetails: function(data) {
                    navigate("playlist", data);
                }
            }

            HorizontalList {
                id: homeTopTracks
                width: parent.width

                title: i18n.tr("Top tracks")
                icon: 'starred'
                uuid: ''
                getSize: function(uuid) {
                    return spotSession.getAlbum(uuid).size;
                } 
                getData: function(uuid, index) {
                    return spotSession.getTrackByAlbum(uuid, index)
                }
                onStart: startTrack(data)
            }
        }
    }

}