import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Pickers 1.0

Page {
    id: settings
    anchors.fill: parent

    property var _needSave: false;

    signal logout()

    Component.onCompleted: {
        console.log("OnComplete", settings.width);
    }

    function init() {
        qualityColumnChoises.currentIndex = spotSession.settings.quality;
        themeColumnChoises.currentIndex = spotSession.settings.theme;
        errorSongColumnChoises.currentIndex = spotSession.settings.errorSong;
    }

    header: PageHeader {
        id: header
        title: i18n.tr("Settings")
        StyleHints {
            dividerColor: UbuntuColors.green
        }
        trailingActionBar.actions: [
            Action {
                visible: _needSave
                text: i18n.tr("Reset")
                iconName: "reset"
                onTriggered: {
                    settings.init();
                    _needSave = false;
                }
            },
            Action {
                visible: _needSave
                text: i18n.tr("Save")
                iconName: "ok"
                onTriggered: {
                    spotSession.saveSettings(qualityColumnChoises.currentIndex, themeColumnChoises.currentIndex, errorSongColumnChoises.currentIndex);

                    if (spotSession.settings.theme === 0) {
                        theme.name = "Ubuntu.Components.Themes.SuruDark"
                    } else if(spotSession.settings.theme === 1) {
                        theme.name = "Ubuntu.Components.Themes.Ambiance"
                    } else {
                        theme.name = ""
                    }

                    _needSave = false;
                }
            }
        ]
    }

    Flickable {
        anchors.top: header.bottom
        width: parent.width
        height: parent.height - header.height
        contentWidth: parent.width
        contentHeight: units.gu(60)

        Column {
            id: columnSettings
            spacing: units.gu(4)
            padding: units.gu(2)
            width: parent.width

            Component.onCompleted: {
                console.log("OnComplete Column", settings.width, parent.width, columnSettings.width);
            }

            Column {
                id: themeColumn
                spacing: units.gu(2)
                padding: units.gu(1)
                width: units.gu(10) * 3
                Row {
                    spacing: units.gu(1)
                    Icon {
                        height: themeLabel.height
                        name: 'audio-x-generic-symbolic'
                    }
                    Label {
                        id: themeLabel
                        text: i18n.tr('Theme:')
                        textSize: Label.Large
                    }
                }
                ListView {
                    id: themeColumnChoises
                    height: units.gu(6)
                    width: units.gu(10) * 3
                    anchors.horizontalCenter: themeColumn.horizontalCenter
                    orientation: ListView.Horizontal
                    model: [i18n.tr('Dark'), i18n.tr('Light'), i18n.tr('System')]
                    delegate: ListItem {
                        width: units.gu(10)
                        height: parent.height
                        divider.visible: false
                        Label {
                            text: modelData
                            anchors.verticalCenter: parent.verticalCenter
                            width: parent.width
                            elide: Text.ElideLeft
                            wrapMode: Text.Wrap
                            maximumLineCount: 3
                            horizontalAlignment: Text.AlignHCenter
                        }
                        onClicked: {
                            themeColumnChoises.currentIndex = index;
                            _needSave = true;
                        }
                    }
                    currentIndex: 1
                    focus: true
                    highlight:  Rectangle {
                        width: themeColumnChoises.width
                        height: units.gu(10)
                        color: UbuntuColors.green
                        y: themeColumnChoises.currentItem == null ? -1 : themeColumnChoises.currentItem.y
                        Behavior on x {
                            SpringAnimation {
                                spring: 3
                                damping: 0.2
                            }
                        }
                    }
                }
            }

            Column {
                id: qualityColumn
                spacing: units.gu(2)
                padding: units.gu(1)
                width: units.gu(10) * 3
                Row {
                    spacing: units.gu(1)
                    Icon {
                        height: qualityLabel.height
                        name: 'audio-x-generic-symbolic'
                    }
                    Label {
                        id: qualityLabel
                        text: i18n.tr('Quality:')
                        textSize: Label.Large
                    }
                }
                ListView {
                    id: qualityColumnChoises
                    height: units.gu(6)
                    width: units.gu(10) * 3
                    anchors.horizontalCenter: qualityColumn.horizontalCenter
                    orientation: ListView.Horizontal
                    model: [i18n.tr('low: prefer data over quality'), i18n.tr('medium'), i18n.tr('high: prefer quality over data')]
                    delegate: ListItem {
                        width: units.gu(10)
                        height: parent.height
                        divider.visible: false
                        Label {
                            text: modelData
                            anchors.verticalCenter: parent.verticalCenter
                            width: parent.width
                            elide: Text.ElideLeft
                            wrapMode: Text.Wrap
                            maximumLineCount: 3
                            horizontalAlignment: Text.AlignHCenter
                        }
                        onClicked: {
                            qualityColumnChoises.currentIndex = index;
                            _needSave = true;
                        }
                    }
                    currentIndex: 1
                    focus: true
                    highlight:  Rectangle {
                        width: qualityColumnChoises.width
                        height: units.gu(10)
                        color: UbuntuColors.green
                        y: qualityColumnChoises.currentItem == null ? -1 : qualityColumnChoises.currentItem.y
                        Behavior on x {
                            SpringAnimation {
                                spring: 3
                                damping: 0.2
                            }
                        }
                    }
                }
            }

            Column {
                id: errorSongColumn
                spacing: units.gu(2)
                padding: units.gu(1)
                width: units.gu(10) * 3
                Row {
                    spacing: units.gu(1)
                    Icon {
                        height: errorSong.height
                        name: 'audio-x-generic-symbolic'
                    }
                    Label {
                        id: errorSong
                        text: i18n.tr('Error song:')
                        textSize: Label.Large
                    }
                }
                ListView {
                    id: errorSongColumnChoises
                    height: units.gu(6)
                    width: units.gu(10) * 2
                    anchors.horizontalCenter: errorSongColumn.horizontalCenter
                    orientation: ListView.Horizontal
                    model: [i18n.tr('silence: 3 seconds'), i18n.tr('error: usefull to know what happens')]
                    delegate: ListItem {
                        width: units.gu(10)
                        height: parent.height
                        divider.visible: false
                        Label {
                            text: modelData
                            anchors.verticalCenter: parent.verticalCenter
                            width: parent.width
                            elide: Text.ElideLeft
                            wrapMode: Text.Wrap
                            maximumLineCount: 3
                            horizontalAlignment: Text.AlignHCenter
                        }
                        onClicked: {
                            errorSongColumnChoises.currentIndex = index;
                            _needSave = true;
                        }
                    }
                    currentIndex: 1
                    focus: true
                    highlight:  Rectangle {
                        width: errorSongColumnChoises.width
                        height: units.gu(10)
                        color: UbuntuColors.green
                        y: errorSongColumnChoises.currentItem == null ? -1 : errorSongColumnChoises.currentItem.y
                        Behavior on x {
                            SpringAnimation {
                                spring: 3
                                damping: 0.2
                            }
                        }
                    }
                }
            }

            Column {
                spacing: units.gu(2)
                padding: units.gu(1)
                width: units.gu(10) * 3
                Row {
                    spacing: units.gu(1)
                    Icon {
                        height: accoutnLabel.height
                        name: 'preferences-desktop-accounts-symbolic'
                    }
                    Label {
                        id: accoutnLabel
                        text: i18n.tr('Account:')
                        textSize: Label.Large
                    }
                }
                Row {
                    width: units.gu(10) * 3
                    layoutDirection: Qt.RightToLeft

                    Button {
                        text: i18n.tr('Log out')
                        onClicked: logout()
                    }
                }
            }
        }
    }
}