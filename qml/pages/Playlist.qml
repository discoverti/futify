import QtQuick 2.12
import Ubuntu.Components 1.3

import "../components"
import "../model"

Page {
    id: page

    property PlaylistType playlist

    signal startPlaylist(PlaylistType playlist)
    signal startTrack(var track)
    signal addToEndQueue(var track)

    header: PageHeader {
        id: header
        title: playlist.name
        StyleHints {
            dividerColor: UbuntuColors.green
        }
    }

    PlaylistView {
        id: playlistView
        playlist: page.playlist
        anchors.top: header.bottom
        height: page.height - header.height
        width: page.width
        onStartPlaylist: page.startPlaylist(playlist)
        onStartTrack: page.startTrack(track)
        onAddToEndQueue: page.addToEndQueue(track)
    }
}