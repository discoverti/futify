import QtQuick 2.12
import Ubuntu.Components 1.3
import QtMultimedia 5.12
import QtGraphicalEffects 1.0

Row {
    
    property var player
    signal showDetails()

    spacing: units.gu(1)
    padding: units.gu(1)

    Item {
        id: playPauseImage
        width: parent.height
        height: parent.height
        anchors.verticalCenter: parent.verticalCenter
        visible: player.playlist.itemCount > 0

        Image {
            id: image
            source: player.playlist.itemCount <= 0 || player.playlist.currentIndex < 0 ? "" : spotSession.getTrackByAlbum('queue', player.playlist.currentIndex).image
            width: parent.width
            height: parent.height
            anchors.verticalCenter: parent.verticalCenter
            fillMode: Image.PreserveAspectFit
            visible: false
        }
        OpacityMask {
            anchors.fill: image
            source: image
            width: image.width
            height: image.height
            maskSource: Rectangle {
                width: image.width
                height: image.height
                radius: 5
                visible: false // this also needs to be invisible or it will cover up the image
            }
        }
        Rectangle {
            color: "#99FFFFFF"
            width: units.gu(3)
            height: units.gu(3)
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            radius: 50
        }
        Icon {
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            name: player.playbackState == Audio.PlayingState ? "media-playback-pause" : "media-playback-start"
            color: "#000000"
            width: units.gu(3)
            height: units.gu(3)
        }
        TapHandler {
            onTapped: {
                if (player.playbackState != Audio.PlayingState) {
                    player.play();
                } else {
                    player.pause();
                }
            }
        }
    }

    Label {
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width - playPauseImage.width - units.gu(18)
        text: player.playlist.itemCount == 0 || player.playlist.currentIndex < 0 ? i18n.tr("No song") : spotSession.getTrackByAlbum('queue', player.playlist.currentIndex).name
        elide: Text.ElideLeft
        wrapMode: Text.Wrap
        maximumLineCount: 1

        TapHandler {
            onTapped: {
                showDetails();
            }
        }
    }

    Icon {
        anchors.verticalCenter: parent.verticalCenter
        visible: player.playlist.itemCount > 0
        name: 'media-seek-backward'
        color: player.playlist.currentIndex > 0 ? "#ffffff" : "#333333"
        width: units.gu(3)
        height: units.gu(3)

        TapHandler {
            onTapped: {
                if (player.playlist.currentIndex > 0) {
                    player.playlist.previous()
                }
            }
        }
    }

    Label {
        anchors.verticalCenter: parent.verticalCenter
        visible: player.playlist.itemCount > 0
        text: Math.min((player.playlist.currentIndex + 1), player.playlist.itemCount) + " / " + player.playlist.itemCount
    }

    Icon {
        anchors.verticalCenter: parent.verticalCenter
        visible: player.playlist.itemCount > 0
        name: 'media-seek-forward'
        color: player.playlist.currentIndex < player.playlist.itemCount - 1 ? "#ffffff" : "#333333"
        width: units.gu(3)
        height: units.gu(3)

        TapHandler {
            onTapped: {
                if (player.playlist.currentIndex < player.playlist.itemCount - 1) {
                    player.playlist.next()
                }
            }
        }
    }

}