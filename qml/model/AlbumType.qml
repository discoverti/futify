import QtQuick 2.12

QtObject {
    property string uuid: ""
    property string image: ""
    property string name: ""
    property string group: ""
    property int size: 0

    function getTrack(index) {
        return spotSession.getTrackByAlbum(uuid, index);
    }
}