import QtQuick 2.12

QtObject {
    property string uuid: ""
    property string image: ""
    property string name: ""
    property string owner: ""
    property int followers: 0
    property int size: 0

    function getTrack(index) {
        return spotSession.getTrackByPlaylist(uuid, index);
    }
}