import QtMultimedia 5.12

MediaPlayer {
    id: playMusic
    autoLoad: true
    autoPlay: false
    audioRole: MediaPlayer.MusicRole

    signal changes()

    playlist: Playlist {
        id: playerPlaylist
        onItemRemoved: {
            console.log("media removed", start, end, playerPlaylist.currentIndex);
            changes();
        }
        onItemInserted: {
            console.log("media inserted", start, end, playerPlaylist.currentIndex);
            changes();
        }
        onCurrentIndexChanged: {
            console.log("current index change", playerPlaylist.currentIndex); 
            changes();
        }
    }
    onPlaybackStateChanged: {
        console.log("player state changed", playMusic.playbackState);
        changes();
    }
    onError: {
        console.log("player error", error, errorString);
    }

    function appendAndPlayTrack(track) {
        console.log('appendAndPlayTrack', track.name);
        spotSession.endQueue(track);
        playerPlaylist.addItem(track.path);
        playerPlaylist.currentIndex = spotSession.getAlbum('queue').size - 1;
        if (playMusic.playbackState != Audio.PlayingState) {
            playMusic.play();
        }
    }

    function appendTrack(track) {
        console.log('appendTrack', track.name);
        spotSession.endQueue(track);
        playerPlaylist.addItem(track.path);
        if (playMusic.playbackState != Audio.PlayingState) {
            playMusic.play();
        }
    }

    function startPlaylist(playlist) {
        spotSession.loadPlaylist(playlist.uuid);
        var playlistLoaded = spotSession.getPlaylist(playlist.uuid);
        var data = [];
        spotSession.clearQueue();
        for (var i=0; i<playlistLoaded.size-1; i++) {
            var track = playlist.getTrack(i);
            data.push(track.path);
            spotSession.endQueue(track);
        }
        playerPlaylist.removeItems(0, playerPlaylist.itemCount - 1);
        playerPlaylist.addItems(data);
        if (playMusic.playbackState != Audio.PlayingState) {
            playMusic.play();
        }
    }

    function startAlbum(album) {
        spotSession.loadAlbum(album.uuid);
        var albumLoaded = spotSession.getAlbum(album.uuid);
        var data = [];
        spotSession.clearQueue();
        for (var i=0; i<albumLoaded.size-1; i++) {
            var track = album.getTrack(i);
            data.push(track.path);
            spotSession.endQueue(track);
        }
        playerPlaylist.removeItems(0, playerPlaylist.itemCount - 1);
        playerPlaylist.addItems(data);
        if (playMusic.playbackState != Audio.PlayingState) {
            playMusic.play();
        }
    }

    function clear() {
        playMusic.stop();
        playerPlaylist.removeItems(0, playerPlaylist.itemCount - 1);
        spotSession.clearQueue();
    }

}