// Heavily inspired by https://gitlab.com/ubports/apps/music-app/-/blob/master/app/components/Helpers/UserMetricsHelper.qml

import QtQuick 2.12
import Ubuntu.Components 1.3
import UserMetrics 0.1


Item {

    property var player

    Metric {
        id: songsMetric
        name: "futify"
        format: "<b>%1</b> " + i18n.tr("tracks played today")
        emptyFormat: i18n.tr("No tracks played today")
        domain: "futify.frenchutouch"
    }

    // Connections for usermetrics
    Connections {
        id: userMetricPlayerConnection
        target: player

        property bool songCounted: false

        onPositionChanged: {
            // Increment song count on Welcome screen if song has been
            // playing for over 10 seconds.
            if (player.position > 10000 && !songCounted) {
                songCounted = true
                songsMetric.increment(1)
                console.debug("Increment UserMetrics")
            }
        }
    }

    Connections {
        target: player.playlist
        onCurrentIndexChanged: userMetricPlayerConnection.songCounted = false
        onCurrentItemSourceChanged: userMetricPlayerConnection.songCounted = false
    }
}
