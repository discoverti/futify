/*
 * Copyright (C) 2020  Romain Maneschi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package main

import (
	"fmt"
	"log"

	"github.com/nanu-c/qml-go"
	"gitlab.com/frenchutouch/futify/server"
)

func main() {
	err := qml.Run(run)
	if err != nil {
		log.Fatal(err)
	}
}

func run() error {

	engine := qml.NewEngine()
	component, err := engine.LoadFile("qml/Main.qml")
	if err != nil {
		return err
	}

	spotSession := server.NewSession(8765)
	player := server.NewHttpPlayer(spotSession)
	player.Start()

	fmt.Println("before create context")
	context := engine.Context()
	fmt.Println("before set spotSession")
	context.SetVar("spotSession", spotSession)

	fmt.Println("before create window")
	win := component.CreateWindow(context)
	fmt.Println("before set win root")
	spotSession.Root = win.Root()

	fmt.Println("before window show")
	win.Show()
	fmt.Println("before window wait")
	win.Wait()

	return nil
}
