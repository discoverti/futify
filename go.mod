module gitlab.com/frenchutouch/futify

go 1.16

require (
	github.com/librespot-org/librespot-golang v0.0.0-20200423180623-b19a2f10c856
	github.com/nanu-c/qml-go v0.0.0-20201002212753-238e81315528
	github.com/zmb3/spotify v1.3.0
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8
)
